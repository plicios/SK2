#include <stdio.h>
#include <string.h>
#include <net/if.h>
#include <sys/ioctl.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <stdlib.h>

#include <arpa/inet.h>
#include <vector>
#include <string>
#include <iostream>


#include <fcntl.h>

std::string GetMac(char* interface_name);
static int GetSvrMacAddress(char* interface_name);
