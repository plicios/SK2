#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <vector>
#include <string>
#include <iostream>
#include <sys/ioctl.h>
#include <net/if.h>
#include <fcntl.h>
#include "Mac.hpp"


#define SERVER_PORT 34113
#define QUEUE_SIZE 20
#define BUFSIZE 100

using namespace std;

bool startsWith(string mainStr, string toMatch)
{
    // std::string::find returns 0 if toMatch is found at starting
    if(mainStr.find(toMatch) == 0)
        return true;
    else
        return false;
}

bool isConnectingString(string mainStr)
{
    return startsWith(mainStr, "connect");
}

bool isShuttingString(string mainStr)
{
    return startsWith(mainStr, "shut");
}

//char buffer[BUFSIZE];

char writeBuffer[BUFSIZE];

volatile sig_atomic_t flag = 0;
void my_function(int sig) // can be called asynchronously
{ 
    flag = 1; // set flag
}

bool SetSocketBlockingEnabled(int fd, bool blocking)
{
   if (fd < 0) return false;

#ifdef _WIN32
   unsigned long mode = blocking ? 0 : 1;
   return (ioctlsocket(fd, FIONBIO, &mode) == 0) ? true : false;
#else
   int flags = fcntl(fd, F_GETFL, 0);
   if (flags == -1) return false;
   flags = blocking ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
   return (fcntl(fd, F_SETFL, flags) == 0) ? true : false;
#endif
}

bool systemShutDown = false;

int main(int argc, char* argv[]){
	//laptop interface name = wlp3s0b1
    signal(SIGINT, my_function);

    string macAddr = "";
    if(argc > 1)
    {
        macAddr = GetMac(argv[1]);
    }
    else
    {
        macAddr = GetMac("eth0");
    }


	
    int nSocket, nClientSocket;
	int nBind, nListen;
	int nFoo = 1;
	socklen_t nTmp;
	struct sockaddr_in stAddr, stClientAddr;
	vector<sockaddr_in> clientsSocketsAddrs;


	/* address structure */
	memset(&stAddr, 0, sizeof(struct sockaddr));
	stAddr.sin_family = AF_INET;
	stAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	stAddr.sin_port = htons(SERVER_PORT);

	/* create a socket */
	nSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (nSocket < 0)
	{
		fprintf(stderr, "%s: Can't create a socket.\n", argv[0]);
		exit(1);
	}

	setsockopt(nSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&nFoo, sizeof(nFoo));
	SetSocketBlockingEnabled(nSocket, false);
	
	/* bind a name to a socket */
	nBind = bind(nSocket, (struct sockaddr*)&stAddr, sizeof(struct sockaddr));
	if (nBind < 0)
	{
		fprintf(stderr, "%s: Can't bind a name to a socket.\n", argv[0]);
		exit(1);
	}

	/* specify queue size */
	nListen = listen(nSocket, QUEUE_SIZE);
	if (nListen < 0)
	{
		fprintf(stderr, "%s: Can't set queue size.\n", argv[0]);
	}

	cout<< "LOGGER: Server started" << endl;

	while(true)
	{
		if(flag == 1)
		{
		    break;
		}
		/* block for connection request */
		nTmp = sizeof(struct sockaddr);
		

		nClientSocket = accept(nSocket, (struct sockaddr*)&stClientAddr, &nTmp);
		if (nClientSocket < 0)
		{
			//fprintf(stderr, "%s: Can't create a connection's socket.\n", argv[0]);
		}
		else
		{
			cout<<"Logger: Incoming connection"<<endl;
			std::string stringToWriteTo = "";
			string buffer (BUFSIZE, '\0');

			
			//while (read (nClientSocket, buffer, BUFSIZE) > 0)
			while (true)
			{
				cout<<"DEBUG: before read data"<<endl;
				int someX = read (nClientSocket, &buffer[0], buffer.capacity());
				cout<<"DEBUG: lettersRead = "<<someX<<endl;
				if(someX <=0)
				{
					cout<<"Logger: End of data"<<endl;
					break;
				}	
				cout<<"Logger: reading data"<<endl;
				string someData = buffer.c_str();
				cout<<someData<<endl;
				stringToWriteTo.append(someData);
				cout<<"DEBUG: string: "<<someData<< " appended to "<<stringToWriteTo<<endl;
				cout<<"DEBUG: string: "<<someData<< " appended to "<<stringToWriteTo<<endl;
			}

			cout << "current data: " << stringToWriteTo << endl;

			if (isConnectingString(stringToWriteTo))
			{
				cout<<"Logger: Client connected"<<endl;
				const char* mac2 = macAddr.c_str();
				int n = sprintf(writeBuffer, "%s", mac2);
				write(nClientSocket, writeBuffer, n);
				clientsSocketsAddrs.push_back(stClientAddr);
			}
			else if (isShuttingString(stringToWriteTo))
			{
				cout<<"Logger: Server shutting"<<endl;
				systemShutDown = true;
				close(nClientSocket);
				break;
			}
			else
			{
				int n = sprintf(writeBuffer, "%s\n", "wrong command");
				write(nClientSocket, writeBuffer, n);
			}
			close(nClientSocket);
		}
	}

    close(nSocket);

	cout<<"DEBUG: Out of while"<<endl;
    int sck;
    for (int i = 0; i < clientsSocketsAddrs.size(); ++i) {
        if ((sck = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP)) >= 0) {
		struct sockaddr_in sck_addr;
		memset(&sck_addr,0, sizeof(sck_addr));
		sck_addr.sin_family = AF_INET;
		inet_aton(inet_ntoa(clientsSocketsAddrs[i].sin_addr), &sck_addr.sin_addr);
		sck_addr.sin_port = htons (5000);
            //SetSocketBlockingEnabled(sck, false);
		int connected = connect (sck, (struct sockaddr*) &sck_addr, sizeof sck_addr);
		cout << connected << endl;
            if (connected >= 0) {
		cout << "DEBUG: connected" <<endl;
                int n = sprintf(writeBuffer, "%s",systemShutDown ? "state shutdown" : "state close");
                write(sck, writeBuffer, n);
            }
		//cout<<inet_ntoa(clientsSocketsAddrs[i].sin_addr)<<endl;
        }
    }

    if(systemShutDown)
    {
	cout<<"DEBUG: trying to shutdown"<<endl;
        //system("shutdown -P now");
    }

	return(0);
}

