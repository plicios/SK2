﻿using System;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Client.Classes;

namespace Client
{
    public partial class MainPage : Form
    {
        bool closeThreads = false;
        protected override void OnClosed(EventArgs e)
        {
            closeThreads = true;
            Servers.Instance.closeThreads = true;
            base.OnClosed(e);
        }

        public void RefreshListView()
        {
            
            ServerList.Items.Clear();
            foreach(Server server in Servers.Instance.List)
            {
                ServerList.Items.Add(server);
            }
        }

        public void RemoveServerFromListView(Server server)
        {
            if (ServerList.Items.Contains(server))
            {
                ServerList.Items.Remove(server);
            }
        }

        Thread listeningThread;

        public MainPage()
        {
            InitializeComponent();
            CenterToScreen();

            Servers.Instance.MainPage = this;
            Servers.Instance.ReadConfigFile();
            //Servers.Instance.FindServers();

            listeningThread = new Thread(() =>
            {
                Listen();
            });
            listeningThread.Start();

            

            ServerList.DrawMode = DrawMode.OwnerDrawFixed;
            ServerList.ItemHeight = 40;
            ServerList.BackColor = Color.FromArgb(50,50,50);
        }

        private void ServerList_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index >= 0)
            {
                // This variable will hold the color of the bottom text - the one saying the count of 
                // the avaliable rooms in your example.
                Brush macBrush;
                bool selected = (e.State & DrawItemState.Selected) == DrawItemState.Selected;
                // Here we override the DrawItemEventArgs to change the color of the selected 
                // item background to one of our preference.
                // I changed to SystemColors.Control, to be more like the list you are trying to reproduce.
                // Also, as I see in your example, the font of the room text part is black colored when selected, and gray
                // colored when not selected. So, we are going to reproduce it as well, by setting the correct color
                // on our variable defined above.
                if (selected)
                {
                    e = new DrawItemEventArgs(e.Graphics, e.Font, e.Bounds,
                        e.Index, e.State ^ DrawItemState.Selected, e.ForeColor, SystemColors.GrayText);

                    macBrush = Brushes.LightGray;
                }
                else
                {
                    macBrush = Brushes.Black;
                }

                // Looking more at your example, i noticed a gray line at the bottom of each item.
                // Lets reproduce that, too.
                Pen linePen = new Pen(SystemBrushes.Control);
                Point lineStartPoint = new Point(e.Bounds.Left, e.Bounds.Height + e.Bounds.Top);
                Point lineEndPoint = new Point(e.Bounds.Width, e.Bounds.Height + e.Bounds.Top);

                e.Graphics.DrawLine(linePen, lineStartPoint, lineEndPoint);

                // Command the event to draw the appropriate background of the item.
                e.DrawBackground();

                // Here you get the data item associated with the current item being drawed.
                Server dataItem = ServerList.Items[e.Index] as Server;

                // Here we will format the font of the part corresponding to the Time text of your list item.
                // You can change to wathever you want - i defined it as a bold font.
                Font timeFont = new Font("Microsoft Sans Serif", 10.0f, FontStyle.Bold);

                // Here you draw the time text on the top of the list item, using the format you defined.
                e.Graphics.DrawString(dataItem.Ip?.ToString() ?? "", timeFont, Brushes.Black, e.Bounds.Left + 3, e.Bounds.Top + 5);

                // Now we draw the avaliable rooms part. First we define our font.
                Font roomsFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular);

                // And, finally, we draw that text.
                e.Graphics.DrawString(dataItem.Mac?.ToString() ?? "", roomsFont, macBrush, e.Bounds.Left + 3, e.Bounds.Top + 23);

                Font stateFont;

                string stateText;
                Brush stateBrush;
                switch (dataItem.State)
                {
                    case Server.ServerState.Online:
                        stateText = "Online";
                        if (selected)
                            stateBrush = Brushes.LightGreen;
                        else
                            stateBrush = Brushes.Green;
                        stateFont = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Bold);
                        break;
                    case Server.ServerState.ShutDown:
                        stateText = "Offline";
                        stateBrush = Brushes.Red;
                        stateFont = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Italic);
                        break;
                    case Server.ServerState.ClosedServerApi:
                        stateText = "Closed";
                        stateBrush = Brushes.Black;
                        stateFont = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Italic);
                        break;
                    default:
                        stateText = "NotKnown";
                        stateBrush = Brushes.Blue;
                        stateFont = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular);
                        break;
                }

                e.Graphics.DrawString(stateText, stateFont, stateBrush, e.Bounds.Left + 150, e.Bounds.Top + 8);
            }
        }


        private Server SelectedItem => ServerList.SelectedItems.Count > 0 ? (Server)ServerList.SelectedItems[0] : null;

        private void AddImage_Click(object sender, EventArgs e)
        {
            AddNewServer addNewServerForm = new AddNewServer();
            addNewServerForm.StartPosition = FormStartPosition.CenterParent;
            addNewServerForm.ShowDialog(this);          
        }

        private void RefreshImage_Click(object sender, EventArgs e)
        {
            Servers.Instance.FindServers();
        }

        private void RemoveImage_Click(object sender, EventArgs e)
        {
            Servers.Instance.RemoveServer(SelectedItem);
        }

        private void ConnectServerButton_Click(object sender, EventArgs e)
        {
            SelectedItem?.Connect();
        }

        private void ShutDownServerButton_Click(object sender, EventArgs e)
        {
            SelectedItem?.ShutDown();
        }

        private void WakeServerButton_Click(object sender, EventArgs e)
        {
            SelectedItem?.Wake();
        }

        Socket serverSocket;

        public void Listen()
        {
            SocketPermission permission = new SocketPermission(NetworkAccess.Accept, TransportType.Tcp, "", SocketPermission.AllPorts);
            IPAddress ipAddr = IPAddress.Any;
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 5000);
            serverSocket = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            serverSocket.Bind(ipEndPoint);
            serverSocket.Listen(10);
            serverSocket.ReceiveTimeout = 5000;
            while (true)
            {
                if (!closeThreads)
                {
                    IAsyncResult result = serverSocket.BeginAccept(AcceptSocket, serverSocket);
                    bool success = result.AsyncWaitHandle.WaitOne(1000, true);
                }
                else
                {
                    break;
                }
            }
        }

        private void AcceptSocket(IAsyncResult asyncResult)
        {
            Socket clientSocket = serverSocket.EndAccept(asyncResult);
            byte[] buffer = new byte[100];
            int dataCount = clientSocket.Receive(buffer);
            string data = Encoding.ASCII.GetString(buffer, 0, dataCount);
            IPAddress addr = (clientSocket.RemoteEndPoint as IPEndPoint).Address;
            Server server = Servers.Instance.List.FirstOrDefault(s => s.Ip.Equals(addr));
            if (server != null)
            {
                Invoke((MethodInvoker)delegate
                {
                    server.SetState(data);
                });
            }
            else
            {
                Server newServer = new Server(addr);
                newServer.SetState(data);
                Servers.Instance.AddServer(newServer);
            }
        }

    }
}
