﻿using System.Net.Sockets;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System;

namespace Client.Classes
{
    public class Server : ListViewItem
    {
        private IPAddress ip;
        private PhysicalAddress mac;
        private ServerState state;
        public int Port => 34113;
        public IPAddress Ip => ip;
        public ServerState State => state;
        public PhysicalAddress Mac => mac;


        public Server(IPAddress ip = null, PhysicalAddress mac = null, ServerState state = ServerState.NotKnown)
        {
            this.ip = ip;
            this.mac = mac;
            this.state = state;
        }

        public enum ServerState
        {
            ShutDown,
            Online,
            NotKnown,
            ClosedServerApi
        }

        private Socket CreateSocket()
        {
            Socket socket = new Socket(ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint endPoint = new IPEndPoint(ip, Port);

            IAsyncResult result = socket.BeginConnect(endPoint, null, null);

            bool success = result.AsyncWaitHandle.WaitOne(100, true);

            if (socket.Connected)
            {
                socket.EndConnect(result);
                return socket;
            }
            else
            {
                socket.Close();
                return null;
            }
        }

        private string Send(string dataToSend)
        {
            if (ip != null)
            {
                Socket socket = CreateSocket();

                if (socket != null)
                {
                    byte[] msg = Encoding.ASCII.GetBytes(dataToSend);
                    int bytesSend = socket.Send(msg);
                    socket.Shutdown(SocketShutdown.Send);

                    byte[] bytes = new byte[100];


                    int bytesRec = socket.Receive(bytes);
                    string theMessageToReceive = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                    socket.Shutdown(SocketShutdown.Both);
                    socket.Dispose();
                    return theMessageToReceive;
                }
            }
            return null;
        }

        public void SetState(string dataRecived)
        {
            if (dataRecived.Equals("state shutdown"))
            {
                state = ServerState.ShutDown;
                Servers.Instance.MainPage.RefreshListView();
                return;
            }
            if (dataRecived.Equals("state online"))
            {
                state = ServerState.Online;
                Servers.Instance.MainPage.RefreshListView();
                return;
            }
            if(dataRecived.Equals("state close"))
            {
                state = ServerState.ClosedServerApi;
                Servers.Instance.MainPage.RefreshListView();
                return;
            }
        }

        

        public void Connect()
        {
            string dataRecived = Send("connect");
            bool success = !string.IsNullOrEmpty(dataRecived);
            string connection = success ? "connected" : "not connected";
            Console.WriteLine($"{ip.ToString()} {connection}");
            if (success)
            {
                dataRecived = dataRecived.ToUpper();
                dataRecived = dataRecived.Replace(":", "");
                Regex reg = new Regex(@"^([0-9A-Fa-f]{2}){6}$");
                if (reg.IsMatch(dataRecived))
                {
                    state = ServerState.Online;
                    mac = PhysicalAddress.Parse(dataRecived);
                    Servers.Instance.MainPage.RefreshListView();
                }
                else
                {
                    //TODO server online but bad response
                }
            }
            else
            {
                //TODO Make popUp with info success = false
            }
        }

        public void ShutDown()
        {
            Send("shut");
        }

        public void Wake()
        {
            if (mac != null)
            {
                byte[] macBytes = mac.GetAddressBytes();

                // WOL packet is sent over UDP 255.255.255.0:40000.
                UdpClient client = new UdpClient();
                client.Connect(IPAddress.Broadcast, 40000);

                // WOL packet contains a 6-bytes trailer and 16 times a 6-bytes sequence containing the MAC address.
                byte[] packet = new byte[17 * 6];

                // Trailer of 6 times 0xFF.
                for (int i = 0; i < 6; i++)
                    packet[i] = 0xFF;

                // Body of magic packet contains 16 times the MAC address.
                for (int i = 1; i <= 16; i++)
                    for (int j = 0; j < 6; j++)
                        packet[i * 6 + j] = macBytes[j];

                // Send WOL packet.
                client.Send(packet, packet.Length);
            }
        }
    }
}
