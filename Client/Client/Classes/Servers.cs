﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace Client.Classes
{
    class Servers
    {
        private static MainPage mainPage;
        public MainPage MainPage
        {
            set => mainPage = value;
            get => mainPage;
        }

        private static Servers instance;
        public static Servers Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new Servers();
                }
                return instance;
            }
        }

        private Servers()
        {
            list = new List<Server>();
        }

        public void ReadConfigFile()
        {
            IEnumerable<string> serverStrings = File.ReadLines(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "config.txt"));
            foreach (string serverString in serverStrings)
            {
                string[] strings = serverString.Split(';');
                strings[1] = strings[1].ToUpper();
                strings[1] = strings[1].Replace(":", "");

                AddServer(new Server(IPAddress.Parse(strings[0]), PhysicalAddress.Parse(strings[1]), Server.ServerState.NotKnown));
            }
        }

        private List<Server> list;
        public List<Server> List => list;

        public void AddServer(Server server)
        {
            list.Add(server);
            mainPage.RefreshListView();
        }

        public void RemoveServer(Server server)
        {
            if (list.Contains(server))
            {
                list.Remove(server);
                mainPage.RemoveServerFromListView(server);
            }
        }

        public bool closeThreads = false;

        Thread findServersThread;
        public void FindServers()
        {
            if (findServersThread == null || !findServersThread.IsAlive)
            {
                findServersThread = new Thread(() =>
                {
                    IPAddress ipAddr = GetLocalIPAddress();
                    IPAddress mask = GetSubnetMask(ipAddr);

                    BitArray maskBits = new BitArray(mask.GetAddressBytes());
                    BitArray ipAddrBits = new BitArray(ipAddr.GetAddressBytes());
                    BitArray startIpAddrBits = ipAddrBits.And(maskBits);
                    startIpAddrBits[startIpAddrBits.Length - 8] = true;

                    IPAddress startIpAddr = new IPAddress(ConvertToBytes(startIpAddrBits));

                    BitArray notMaskBits = maskBits.Not();
                    BitArray endIpAddrBits = ipAddrBits.Or(notMaskBits);
                    endIpAddrBits[endIpAddrBits.Length - 8] = false;
                    IPAddress endIpAddr = new IPAddress(ConvertToBytes(endIpAddrBits));

                    RangeFinder rangeFinder = new RangeFinder();
                    List<IPAddress> ipList = rangeFinder.GetIPRange(startIpAddr, endIpAddr);
                    foreach (IPAddress ipAddress in ipList)
                    {
                        if (closeThreads)
                        {
                            break;
                        }
                        Server serverTest = list.Where(serv => serv.Ip.Equals(ipAddress)).FirstOrDefault();
                        if (serverTest == null)
                        {
                            Server server = new Server(ipAddress);
                            server.Connect();

                            if (server.State == Server.ServerState.Online)
                            {
                                mainPage.Invoke((MethodInvoker)delegate
                                {
                                    AddServer(server);
                                });
                            }
                        }
                        else
                        {
                            if (serverTest.State != Server.ServerState.Online)
                            {
                                mainPage.Invoke((MethodInvoker)delegate
                                {
                                    serverTest.Connect();
                                });
                            }
                        }
                    }
                });
                findServersThread.Start();
            }
        }

        

        private byte[] ConvertToBytes(BitArray bits)
        {
            byte[] bytes = new byte[4];
            bits.CopyTo(bytes, 0);
            return bytes;
        }

        private static IPAddress GetSubnetMask(IPAddress address)
        {
            foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
            {
                foreach (UnicastIPAddressInformation unicastIPAddressInformation in adapter.GetIPProperties().UnicastAddresses)
                {
                    if (unicastIPAddressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        if (address.Equals(unicastIPAddressInformation.Address))
                        {
                            return unicastIPAddressInformation.IPv4Mask;
                        }
                    }
                }
            }
            throw new ArgumentException(string.Format("Can't find subnetmask for IP address '{0}'", address));
        }

        private IPAddress GetLocalIPAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip;
                }
            }
            Console.WriteLine("No network adapters with an IPv4 address in the system!");
            return null;
        }
    }
}
