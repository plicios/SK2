﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;

public class RangeFinder
{
    public List<IPAddress> GetIPRange(IPAddress startIP, IPAddress endIP)
    {
        List<IPAddress> ipList = new List<IPAddress>();
        uint sIP = ipToUint(startIP.GetAddressBytes());
        uint eIP = ipToUint(endIP.GetAddressBytes());
        while (sIP <= eIP)
        {
            ipList.Add(new IPAddress(reverseBytesArray(sIP)));
            sIP++;
        }
        return ipList;
    }


    /* reverse byte order in array */
    protected uint reverseBytesArray(uint ip)
    {
        byte[] bytes = BitConverter.GetBytes(ip);
        bytes = bytes.Reverse().ToArray();
        return (uint)BitConverter.ToInt32(bytes, 0);
    }


    /* Convert bytes array to 32 bit long value */
    protected uint ipToUint(byte[] ipBytes)
    {
        ByteConverter bConvert = new ByteConverter();
        uint ipUint = 0;

        int shift = 24; // indicates number of bits left for shifting
        foreach (byte b in ipBytes)
        {
            if (ipUint == 0)
            {
                ipUint = (uint)bConvert.ConvertTo(b, typeof(uint)) << shift;
                shift -= 8;
                continue;
            }

            if (shift >= 8)
                ipUint += (uint)bConvert.ConvertTo(b, typeof(uint)) << shift;
            else
                ipUint += (uint)bConvert.ConvertTo(b, typeof(uint));

            shift -= 8;
        }

        return ipUint;
    }
}