﻿namespace Client
{
    partial class AddNewServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IpText = new System.Windows.Forms.TextBox();
            this.MacAddress = new System.Windows.Forms.TextBox();
            this.AddServerButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // IpText
            // 
            this.IpText.BackColor = System.Drawing.SystemColors.GrayText;
            this.IpText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IpText.Location = new System.Drawing.Point(45, 12);
            this.IpText.Name = "IpText";
            this.IpText.Size = new System.Drawing.Size(100, 20);
            this.IpText.TabIndex = 0;
            // 
            // MacAddress
            // 
            this.MacAddress.BackColor = System.Drawing.SystemColors.GrayText;
            this.MacAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MacAddress.Location = new System.Drawing.Point(45, 38);
            this.MacAddress.Name = "MacAddress";
            this.MacAddress.Size = new System.Drawing.Size(100, 20);
            this.MacAddress.TabIndex = 1;
            // 
            // AddServerButton
            // 
            this.AddServerButton.BackColor = System.Drawing.SystemColors.GrayText;
            this.AddServerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddServerButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AddServerButton.Location = new System.Drawing.Point(66, 64);
            this.AddServerButton.Name = "AddServerButton";
            this.AddServerButton.Size = new System.Drawing.Size(60, 25);
            this.AddServerButton.TabIndex = 2;
            this.AddServerButton.Text = "Add";
            this.AddServerButton.UseVisualStyleBackColor = false;
            this.AddServerButton.Click += new System.EventHandler(this.AddServerButton_Click);
            // 
            // AddNewServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(184, 111);
            this.Controls.Add(this.AddServerButton);
            this.Controls.Add(this.MacAddress);
            this.Controls.Add(this.IpText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddNewServer";
            this.Text = "AddNewServer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox IpText;
        private System.Windows.Forms.TextBox MacAddress;
        private System.Windows.Forms.Button AddServerButton;
    }
}