﻿namespace Client
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.ConnectServerButton = new System.Windows.Forms.Button();
            this.ShutDownServerButton = new System.Windows.Forms.Button();
            this.WakeServerButton = new System.Windows.Forms.Button();
            this.RemoveImage = new System.Windows.Forms.PictureBox();
            this.RefreshImage = new System.Windows.Forms.PictureBox();
            this.AddImage = new System.Windows.Forms.PictureBox();
            this.ServerList = new System.Windows.Forms.ListBox();
            this.serversBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.RemoveImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RefreshImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serversBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ConnectServerButton
            // 
            this.ConnectServerButton.BackColor = System.Drawing.SystemColors.GrayText;
            this.ConnectServerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConnectServerButton.Location = new System.Drawing.Point(12, 412);
            this.ConnectServerButton.Name = "ConnectServerButton";
            this.ConnectServerButton.Size = new System.Drawing.Size(75, 23);
            this.ConnectServerButton.TabIndex = 2;
            this.ConnectServerButton.Text = "Connect";
            this.ConnectServerButton.UseVisualStyleBackColor = false;
            this.ConnectServerButton.Click += new System.EventHandler(this.ConnectServerButton_Click);
            // 
            // ShutDownServerButton
            // 
            this.ShutDownServerButton.BackColor = System.Drawing.SystemColors.GrayText;
            this.ShutDownServerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ShutDownServerButton.Location = new System.Drawing.Point(12, 441);
            this.ShutDownServerButton.Name = "ShutDownServerButton";
            this.ShutDownServerButton.Size = new System.Drawing.Size(75, 23);
            this.ShutDownServerButton.TabIndex = 3;
            this.ShutDownServerButton.Text = "Shutdown";
            this.ShutDownServerButton.UseVisualStyleBackColor = false;
            this.ShutDownServerButton.Click += new System.EventHandler(this.ShutDownServerButton_Click);
            // 
            // WakeServerButton
            // 
            this.WakeServerButton.BackColor = System.Drawing.SystemColors.GrayText;
            this.WakeServerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WakeServerButton.Location = new System.Drawing.Point(12, 470);
            this.WakeServerButton.Name = "WakeServerButton";
            this.WakeServerButton.Size = new System.Drawing.Size(75, 23);
            this.WakeServerButton.TabIndex = 6;
            this.WakeServerButton.Text = "Wake";
            this.WakeServerButton.UseVisualStyleBackColor = false;
            this.WakeServerButton.Click += new System.EventHandler(this.WakeServerButton_Click);
            // 
            // RemoveImage
            // 
            this.RemoveImage.Image = global::Client.Properties.Resources.remove;
            this.RemoveImage.InitialImage = ((System.Drawing.Image)(resources.GetObject("RemoveImage.InitialImage")));
            this.RemoveImage.Location = new System.Drawing.Point(64, 72);
            this.RemoveImage.Name = "RemoveImage";
            this.RemoveImage.Size = new System.Drawing.Size(23, 24);
            this.RemoveImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RemoveImage.TabIndex = 7;
            this.RemoveImage.TabStop = false;
            this.RemoveImage.WaitOnLoad = true;
            this.RemoveImage.Click += new System.EventHandler(this.RemoveImage_Click);
            // 
            // RefreshImage
            // 
            this.RefreshImage.Image = global::Client.Properties.Resources.refresh2;
            this.RefreshImage.InitialImage = ((System.Drawing.Image)(resources.GetObject("RefreshImage.InitialImage")));
            this.RefreshImage.Location = new System.Drawing.Point(64, 42);
            this.RefreshImage.Name = "RefreshImage";
            this.RefreshImage.Size = new System.Drawing.Size(23, 24);
            this.RefreshImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RefreshImage.TabIndex = 5;
            this.RefreshImage.TabStop = false;
            this.RefreshImage.WaitOnLoad = true;
            this.RefreshImage.Click += new System.EventHandler(this.RefreshImage_Click);
            // 
            // AddImage
            // 
            this.AddImage.Image = global::Client.Properties.Resources.add;
            this.AddImage.InitialImage = ((System.Drawing.Image)(resources.GetObject("AddImage.InitialImage")));
            this.AddImage.Location = new System.Drawing.Point(64, 12);
            this.AddImage.Name = "AddImage";
            this.AddImage.Size = new System.Drawing.Size(23, 24);
            this.AddImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.AddImage.TabIndex = 4;
            this.AddImage.TabStop = false;
            this.AddImage.WaitOnLoad = true;
            this.AddImage.Click += new System.EventHandler(this.AddImage_Click);
            // 
            // ServerList
            // 
            this.ServerList.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ServerList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ServerList.FormattingEnabled = true;
            this.ServerList.Location = new System.Drawing.Point(93, 12);
            this.ServerList.Name = "ServerList";
            this.ServerList.Size = new System.Drawing.Size(260, 483);
            this.ServerList.TabIndex = 8;
            this.ServerList.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.ServerList_DrawItem);
            // 
            // serversBindingSource
            // 
            this.serversBindingSource.DataSource = typeof(Client.Classes.Servers);
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(359, 505);
            this.Controls.Add(this.ServerList);
            this.Controls.Add(this.RemoveImage);
            this.Controls.Add(this.WakeServerButton);
            this.Controls.Add(this.RefreshImage);
            this.Controls.Add(this.AddImage);
            this.Controls.Add(this.ShutDownServerButton);
            this.Controls.Add(this.ConnectServerButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainPage";
            this.Text = "Client";
            ((System.ComponentModel.ISupportInitialize)(this.RemoveImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RefreshImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serversBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button ShutDownServerButton;
        private System.Windows.Forms.PictureBox AddImage;
        private System.Windows.Forms.PictureBox RefreshImage;
        private System.Windows.Forms.Button ConnectServerButton;
        private System.Windows.Forms.Button WakeServerButton;
        private System.Windows.Forms.PictureBox RemoveImage;
        private System.Windows.Forms.ListBox ServerList;
        private System.Windows.Forms.BindingSource serversBindingSource;
    }
}

