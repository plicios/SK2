﻿using Client.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class AddNewServer : Form
    {
        public AddNewServer()
        {
            InitializeComponent();
        }

        private void AddServerButton_Click(object sender, EventArgs e)
        {
            IPAddress ip = null;
            PhysicalAddress mac = null;
            if (!string.IsNullOrEmpty(IpText.Text))
            {
                bool isIp = IPAddress.TryParse(IpText.Text, out ip);
                //TODO
                //Comment if wrong ip format
            }
            if (!string.IsNullOrEmpty(MacAddress.Text))
            {
                MacAddress.Text = MacAddress.Text.ToUpper();
                MacAddress.Text = MacAddress.Text.Replace(":", "");
                mac = PhysicalAddress.Parse(MacAddress.Text);
            }

            Server newServer = new Server(ip, mac);

            Servers.Instance.AddServer(newServer);

            Close();
        }
    }
}
