#include "Mac.hpp"


using namespace std;

//
// Global public data
//
unsigned char cMacAddr[8]; // Server's MAC address

static int GetSvrMacAddress(char* interface_name)
{
	int nSD; // Socket descriptor
	struct ifreq sIfReq; // Interface request
	struct if_nameindex *pIfList; // Ptr to interface name index
	struct if_nameindex *pListSave; // Ptr to interface name index

	//
	// Initialize this function
	//
	pIfList = (struct if_nameindex *)NULL;
	pListSave = (struct if_nameindex *)NULL;
	#ifndef SIOCGIFADDR
	// The kernel does not support the required ioctls
	return( 0 );
	#endif

	//
	// Create a socket that we can use for all of our ioctls
	//
	nSD = socket( PF_INET, SOCK_STREAM, 0 );
	if ( nSD < 0 )
	{
	// Socket creation failed, this is a fatal error
	printf( "File %s: line %d: Socket failed\n", __FILE__, __LINE__ );
	return( 0 );
	}

	//
	// Obtain a list of dynamically allocated structures
	//
	pIfList = pListSave = if_nameindex();

	//
	// Walk thru the array returned and query for each interface's
	// address
	//
	for ( pIfList; *(char *)pIfList != 0; pIfList++ )
	{
	//
	// Determine if we are processing the interface that we
	// are interested in
	//
	if ( strcmp(pIfList->if_name, interface_name) )
	// Nope, check the next one in the list
	continue;
	strncpy( sIfReq.ifr_name, pIfList->if_name, IF_NAMESIZE );

	//
	// Get the MAC address for this interface
	//
	if ( ioctl(nSD, SIOCGIFHWADDR, &sIfReq) != 0 )
	{
	// We failed to get the MAC address for the interface
	printf( "File %s: line %d: Ioctl failed\n", __FILE__, __LINE__ );
	return( 0 );
	}
	memmove( (void *)&cMacAddr[0], (void *)&sIfReq.ifr_ifru.ifru_hwaddr.sa_data[0], 6 );
	break;
	}

	//
	// Clean up things and return
	//
	if_freenameindex( pListSave );
	close( nSD );
	return( 1 );
}

string GetMac(char* interface_name)
{
	//
	// Initialize this program
	//
	bzero((void *)&cMacAddr[0], sizeof(cMacAddr));
	if (!GetSvrMacAddress(interface_name))
	{
		GetSvrMacAddress("eth0");
		// We failed to get the local host's MAC address
		printf( "Fatal error: Failed to get local host's MAC address\n" );
	}
	
	char buf [20];
	int x = snprintf ( buf, 20, "%02X:%02X:%02X:%02X:%02X:%02X", cMacAddr[0], cMacAddr[1], cMacAddr[2], cMacAddr[3], cMacAddr[4], cMacAddr[5] );
	string newMac(buf);
	
	return newMac;
}
